Hey, I'm Brian. Thanks for stopping by. I'm an independent software developer working on some projects of my own.

## About Me

I live in South Jordan, Utah, where I love to hike, climb, snowboard, canyoneer, backpack, kayak, and just about anything else you can do with four limbs. When I'm not out adventuring, I enjoy using my mind to create new things – sometimes that means making or writing music, and often that means software development.

I studied Computer Science in school, and I've been working in software development since April of 2008 – learning programming on the job a few months before I took my first CS class. I guess 12 years almost makes me a dinosaur in the software world, but I have never stopped or lost the passion for learning new things every day.

Most of the work that I do is web development in various forms. I enjoy coming up with innovative and unique projects where I can apply my entire unique skillset to solving a problem that hasn't been solved before.

## Projects

### Wunderfold

On January 3, 2022, I saw a Facebook post by a friend where she showed a craft she had discovered called book folding. It's a form of art made by cutting and/or folding the edges of a book to produce designs. She mentioned that she purchased patterns to make them, and I immediately became curious about the software used to the create the patterns. I suspected that there might be room for improvement, and thanks to my work creating software for laser designs, I might have just the right skill set to create a better book folding software.

I did some quick internet searches and confirmed my suspicions: it seemed that all the current software was dated, clunky, and expensive. In a few hours I had written an algorithm to turn images into folding patterns, and created a very simple prototype. I decided it was worth putting some time into, and six weeks later, announced publicly the availability of [Wunderfold](https://wunderfold.art/) as beta software. In another couple of weeks I took my first paying customers, and since then have grown Wunderfold into a successful side business, with over 700 members in the [Facebook group](https://facebook.com/groups/wunderfold), and hundreds of subscribers so far.

### Wasatch Lasercraft

Since 2021 I have been involved in the laser cutting engraving business in various ways, though my focus is on creating tools and resources to help support other small laser business owners. My biggest focus has been on creating small standalone software programs that allow laser owners who are not graphic designers to create customized designs for themselves and their customers in a few simple steps. You can learn more about that area of work at [Wasatch Lasercraft.](https://wasatchlasercraft.com/)

### Let’s Code!

In 2019 I create a programming class for kids that I developed both the curriculum and the learning platform for. The first version was taught in person, and the second version was going to be fully online and self-paced, but has been placed on the backburner for other projects. You can check out the original course at [code.mortensoncreative.com](https://code.mortensoncreative.com/), though currently you need a registration code from me to use it.

While developing the course, I found the code environment that I created for the class to be a lot of fun to use for quick canvas-based demo projects, and wrote quite a few of them purely for my own entertainment or to entertain my students, in addition to the examples that I wrote for the class.  I have made some of the more interesting ones publicly visible, and they’re fun to play with:

- [DRUMS](https://code.mortensoncreative.com/ahsj/published/l9hl2dcz) - A mouse and keyboard-playable drum machine using the sound library I built.
- [Dodgeball](https://code.mortensoncreative.com/ahsj/published/dyu2ts4z) - A simple, fun dino game.
- [Blerp 3000](https://code.mortensoncreative.com/ahsj/published/d2um9s50) - I built this to help generate sound effects to use in other games (see King Arthur).
- [Downhill Dino](https://code.mortensoncreative.com/ahsj/published/ydzmzn0z) - Like Chrome’s offline dino game, but downhill, on a snowboard.
- [Important Concept](https://code.mortensoncreative.com/ahsj/published/v0xejy0e) - This one you’ll have to see to understand.
- [Ultra Pong](https://code.mortensoncreative.com/ahsj/published/42psp88k) - Play pong against some AIs. This was the first draft of a bonus project that I created for my students to code their own pong AIs.
- [King Arthur](https://code.mortensoncreative.com/ahsj/published/e2mpymcq) - Slay dragons.
- [Sokoban](https://code.mortensoncreative.com/ahsj/published/4rpgkcd9) - A Sokoban engine with some levels that I found online.
- [Fly Swatter](https://code.mortensoncreative.com/ahsj/published/iekh9jj0) - Way more fun than it should be.

Click the run button to see the project in action. The code is also editable. I built the coding environment myself, and created built-in canvas, animation, and music libraries to use in the environment for the students to use to simplify the creation process.  Logged-in students could create, save, and share projects of their own, in addition to using the course materials that I created.

### Other Projects
I have a lot of silly little games and other things I've made for my own education and entertainment. For example, I used [trio](https://trio.mortensoncreative.com), a game idea I had wanted to create, to teach myself React somewhere around 2015. Shortly after that, I started using React at work.

Feel free to explore these projects - they're mostly self-explanatory. Some are more finished than others.

- [64](https://64.mortensoncreative.com) - A "magic" trick that I invented, that's really just math.
- [Tiles](https://tiles.mortensoncreative.com) - A fun sliding picture game I invented. Implemented in under 200 lines of React.
- [Trio](https://trio.mortensoncreative.com) - A virtual implementation of the game Set.
- [Grams](https://grams.mortensoncreative.com) - prototype for a word puzzle - try to use all the letters to say something coherent.
- [Bronco colorizer](https://colorizer.mortensoncreative.com) - visualize what a custom-painted or wrapped Bronco would look like.

### Resume

If you're interested in hiring me, please take a look at my current
[resume](https://resume.mortensoncreative.com).
